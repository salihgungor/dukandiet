import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BesinlerComponent } from './besinler.component';

describe('BesinlerComponent', () => {
  let component: BesinlerComponent;
  let fixture: ComponentFixture<BesinlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BesinlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BesinlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
