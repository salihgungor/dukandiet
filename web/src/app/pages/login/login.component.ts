import { Component, OnInit } from '@angular/core';
import {DietService} from '../../shared/services/diet.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  constructor(private dietService: DietService, private router: Router) { }

  ngOnInit() {
    this.myForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required),
    });
  }

  onSubmit(){
    this.dietService.login({username: this.myForm.value.username, password: this.myForm.value.password})
      .subscribe(data => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('userId', data.userId);
        localStorage.setItem('name', data.name);
        localStorage.setItem('username', data.username);
        this.router.navigateByUrl('/');
      });
      this.myForm.reset();
  }

}
