import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './pages.routing';

import { LayoutModule } from '../shared/layout.module';
import { SharedModule } from '../shared/shared.module';

/* components */
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { TariflerComponent } from './tarifler/tarifler.component';
import { BesinlerComponent } from './besinler/besinler.component';
import {HakkindaComponent} from './hakkinda/hakkinda.component';
import {DietService} from '../shared/services/diet.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        PagesComponent,
        LoginComponent,
        HomeComponent,
        TariflerComponent,
        BesinlerComponent,
        HakkindaComponent
    ],
    providers: [
        DietService
    ]
})
export class PagesModule { }
