import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TariflerComponent } from './tarifler.component';

describe('TariflerComponent', () => {
  let component: TariflerComponent;
  let fixture: ComponentFixture<TariflerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TariflerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TariflerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
