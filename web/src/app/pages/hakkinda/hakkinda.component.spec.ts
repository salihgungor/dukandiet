import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HakkNdaComponent } from './hakkinda.component';

describe('HakkNdaComponent', () => {
  let component: HakkNdaComponent;
  let fixture: ComponentFixture<HakkNdaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HakkNdaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HakkNdaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
