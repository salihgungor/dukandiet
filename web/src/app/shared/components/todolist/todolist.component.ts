import { Component, OnInit } from '@angular/core';
import { TodoListService } from './todolist.service';
import {DietService} from '../../services/diet.service';
@Component({
  selector: 'du-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss'],
  providers: [TodoListService]
})

export class TodolistComponent implements OnInit {

  todolist = [];
  newTaskText: string;

  sectionName: string;
  dietDay: string;
  totalDietDay: number;
  result;

  constructor(private todoListService: TodoListService, private dietService: DietService) { }

  ngOnInit() {
    // this.todolist = this.todoListService.getTodoList();
    // this.todolist.forEach(item => {
    //   item.isOver = false;
    //   item.isEdit = false;
    //   item.editText = item.text;
    // });

    this.dietService.getUser(localStorage.getItem('username'))
        .subscribe(result => {
          this.createList(result);
        });
  }

  edit(index) {
    if (!this.todolist[index].isOver) {
      this.todolist[index].editText = this.todolist[index].text;
      this.todolist[index].isEdit = true;
    }
  }

  overMatter(index) {
    // if (!this.todolist[index].isEdit) {
    //   this.todolist[index].status = !this.todolist[index].status;
    // }
    this.todolist[index].status = !this.todolist[index].status;
    if (this.totalDietDay <= this.result.diet[0].atack.length) {
      this.result.diet[0].atack[this.totalDietDay].content[index].status = this.todolist[index].status;
    } else if (this.totalDietDay <= (this.result.diet[1].cruise.length+this.result.diet[0].atack.length)) {
      this.result.diet[1].cruise[this.totalDietDay-this.result.diet[0].atack.length].content[index].status = this.todolist[index].status;
    } else if (this.totalDietDay <= (this.result.diet[1].cruise.length+this.result.diet[0].atack.length+this.result.diet[2].consolidation.length)) {
      this.result.diet[2].consolidation[this.totalDietDay-(this.result.diet[1].cruise.length+this.result.diet[0].atack.length)].content[index].status = this.todolist[index].status;
    } else if (this.totalDietDay <= (this.result.diet[1].cruise.length+this.result.diet[0].atack.length+this.result.diet[2].consolidation.length+this.result.diet[3].stabilisation.length)) {
      this.result.diet[3].stabilisation[this.totalDietDay-(this.result.diet[1].cruise.length+this.result.diet[0].atack.length+this.result.diet[2].consolidation.length)].content[index].status = this.todolist[index].status;
    }
    this.dietService.updateDiet(this.result, localStorage.getItem('username'))
      .subscribe(result => {
        this.createList(result);
      });
  }

  enterTaskEdit(index) {
    this.todolist[index].text = this.todolist[index].editText;
    this.todolist[index].isEdit = false;
  }

  cancelTaskEdit(index) {
    this.todolist[index].isEdit = false;
  }

  creatNewTask() {
    const newTask = new List;
    newTask.isEdit = false;
    newTask.isOver = false;
    newTask.text = this.newTaskText;
    this.todolist.unshift(newTask);
  }

  createList(result) {
    const nowDate = new Date();
    const startingDate = new Date(result.startingDate);
    const dietDay = nowDate.getDate()-startingDate.getDate();
    this.dietDay = (dietDay+1)+'. Gün';
    this.totalDietDay = dietDay;
    this.result = result;
    if ((dietDay+1) <= result.diet[0].atack.length) {
      this.sectionName = 'Atak Evresi';
      this.todolist = result.diet[0].atack[dietDay].content;
    } else if ((dietDay+1) <= (result.diet[1].cruise.length+result.diet[0].atack.length)) {
      this.sectionName = 'Seyir Evresi';
      this.todolist = result.diet[1].cruise[dietDay-result.diet[0].atack.length].content;
    } else if ((dietDay+1) <= (result.diet[1].cruise.length+result.diet[0].atack.length+result.diet[2].consolidation.length)) {
      this.sectionName = 'Takviye Evresi';
      this.todolist = result.diet[2].consolidation[dietDay-(result.diet[1].cruise.length+result.diet[0].atack.length)].content;
    } else if ((dietDay+1) <= (result.diet[1].cruise.length+result.diet[0].atack.length+result.diet[2].consolidation.length+result.diet[3].stabilisation.length)) {
      this.sectionName = 'Dengeleme Evresi';
      this.todolist = result.diet[3].stabilisation[dietDay-(result.diet[1].cruise.length+result.diet[0].atack.length+result.diet[2].consolidation.length)].content;
    }
  }

}
export class List {
  text: string;
  editText: string;
  isOver: boolean;
  isEdit: boolean;
}
