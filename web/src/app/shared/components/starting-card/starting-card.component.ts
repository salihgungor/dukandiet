import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DietService} from '../../services/diet.service';
import {RootComponent} from '../../roots/root.component';
import {GlobalService} from '../../services/global.service';
import {ChartsService} from '../../../pages/charts/components/echarts/charts.service';

@Component({
  selector: 'app-starting-card',
  templateUrl: './starting-card.component.html',
  styleUrls: ['./starting-card.component.scss'],
  providers: [ChartsService]
})
export class StartingCardComponent extends RootComponent implements OnInit {
  avatarImgSrc: string = '/images/avatar.png';
  avatarImgSrc2: string = '/images/log.png';

  name: String;
  lastName: String;
  email: String;
  gender: String = 'Bay';
  birthData: Date;
  height: String;
  nowWeight: number;
  targetWeight: number;
  highestWeight: number;
  lowestWeight: number;
  averageWeight: number;
  familyFat: String = 'Evet';
  howManyDietBefore: String = '0';
  username: String;
  password: String;

  showloading: boolean = false;

  public LineOption;
  public PieOption;

  user;

  atack = [];
  cruise = [];
  consolidation = [];
  stabilisation = [];

  @ViewChild('myModal') myModal;
  constructor(public _globalService: GlobalService,
              private dietService: DietService,
              private chartsService: ChartsService) {
      super(_globalService);
  }

  ngOnInit() {
      this.dietService.getUser(localStorage.getItem('username'))
          .subscribe(result => {
            this.user = result;
            this.getPieOption(result.diet[0].atack.length, result.diet[1].cruise.length, result.diet[2].consolidation.length, result.diet[3].stabilisation.length);
            this.LineOption = this.chartsService.getLineOptionDiet(result);
          });
      this.createBackbone();
  }

  createDiet() {
    let k = 6;
    for (let i = 0; i < (this.nowWeight-this.targetWeight-1); i++) {
      for (let j = 0; j < 5; j++) {
          this.consolidation.push(this.consolidation[j]);
          // this.consolidation[k-1].day = k + '. gün';
      }
    }

    // for (let i = 0; i < (this.nowWeight - this.targetWeight) * 5; i++) {
    //     console.log('result: ' + JSON.stringify(this.consolidation[i]));
    // }
    const diet = [
        { atack: this.atack },
        { cruise: this.cruise },
        { consolidation: this.consolidation },
        { stabilisation: this.stabilisation }
    ];

    this.dietService.createDiet({
      name: this.name,
      lastName: this.lastName,
      email: this.email,
      gender: this.gender,
      birthData: this.birthData,
      height: this.height,
      nowWeight: this.nowWeight,
      targetWeight: this.targetWeight,
      highestWeight: this.highestWeight,
      lowestWeight: this.lowestWeight,
      averageWeight: this.averageWeight,
      familyFat: this.familyFat,
      howManyDietBefore: this.howManyDietBefore,
      username: this.username,
      password: this.password,
      startingDate: new Date(),
      diet
    })
    .subscribe(result => {
      this.myModal.hide();
      this.alertMessage({
        type: 'success',
        title: 'Kayıt başarılı',
        value: result.message
      });
    });
  }

  changedGender(gender) {
    this.gender = gender.target.value;
  }

  changedFamilyFat(familyFat) {
    this.familyFat = familyFat.target.value;
  }

  changedHowManyDietBefore(dietCount) {
    this.howManyDietBefore = dietCount.target.value;
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

  getPieOption(atak, seyir, takviye, dengeleme) {
    this.PieOption = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
      },
      legend: {
        orient: 'vertical',
        x: 'left',
        data: ['Atak Evresi', 'Seyir Evresi', 'Takviye Evresi', 'Dengeleme Evresi']
      },
      roseType: 'angle',
      series: [
        {
          name: 'Diet Evreleri Günlük Dağılımı',
          type: 'pie',
          radius: [0, '50%'],
          data: [
            { value: atak, name: 'Atak Evresi' },
            { value: seyir, name: 'Seyir Evresi' },
            { value: takviye, name: 'Takviye Evresi' },
            { value: dengeleme, name: 'Dengeleme Evresi' }
          ]
        }
      ]
    }
  }

  createBackbone() {
      this.atack = [
          {
              day: '1. gün',
              content: [
                  { food: 'Kahvaltı: 2 adet yumurta', status: false },
                  { food: 'Kahvaltı: Yağsız süt', status: false },
                  { food: 'Kahvaltı: Füme somon ile hazırlanmış yağsız yumurta', status: false },
                  { food: 'Öğlen: Yağsız beyaz peynir ile hazırlanmış jambon ruloları + Sade quark', status: false },
                  { food: 'Akşam: Izgara Dana eti (3 damla sıvı yağ ile  hazırlanmış) (soya sosu, zencefil, sarımsak, karanfil, karabiber ile sos hazırlanabilir)', status: false }
              ]
          },
          {
              day: '2. gün',
              content: [
                  { food: 'Kahvaltı: 200 gr yağsız yoğurt + 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Öğlen: 200 gr Kuşbaşı doğranmış tavuk + sade quark(1,5 yemek kaşığı yulaf ezmesi eklenmiş)', status: false },
                  { food: 'Akşam: Izgara Balık (3 damla sıvı yağ ile  hazırlanmış) + doğranmış otlar', status: false },
              ]
          },
          {
              day: '3. gün',
              content: [
                  { food: 'Kahvaltı: 2 yemek kaşığı Yulaf ezmesi ve yağsız süt ile hazırlanmış karışım (tatlandırıcı eklenebilir)', status: false },
                  { food: 'Öğlen: 175 gr kıyma ile hazırlanmış kıymalı yumurta', status: false },
                  { food: 'Akşam: 3 adet yumurta (3 damla sıvı yağ ile  hazırlanmış) + Sade quark', status: false },
              ]
          },
          {
              day: '4. gün',
              content: [
                  { food: 'Kahvaltı: 2 yumurta', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: 2 yumurta ile hazırlanmış 1 yemek kaşığı zeytinyağlı salata', status: false },
                  { food: 'Akşam: 200 gr somon balığı', status: false },
                  { food: 'Akşam: Buğulama Kuşkonmaz', status: false },
              ]
          }
      ];
      this.cruise = [
          {
              day: '1. gün',
              content: [
                  { food: 'Kahvaltı: Dukan Baharatlı Ekmek Fajita', status: false },
                  { food: 'Kahvaltı: 2 yumurta', status: false },
                  { food: 'Öğlen: Fırında hindili mantar', status: false },
                  { food: 'Öğlen: Chia salata', status: false },
                  { food: 'Akşam: Yeşil biberli tavuk dolması)', status: false },
                  { food: 'Akşam: Pişmiş kabak ve havuç', status: false },
              ]
          },
          {
              day: '2. gün',
              content: [
                  { food: 'Kahvaltı: Fajitas Tako Omlet', status: false },
                  { food: 'Öğlen: Pepato, kremalı morbido brokoli', status: false },
                  { food: 'Akşam: Sebzeli mantarlı meze', status: false },
                  { food: 'Akşam: Kapari turşulu ton salata', status: false },
              ]
          },
          {
              day: '3. gün',
              content: [
                  { food: 'Kahvaltı: 200 gr yağsız yoğurt + 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Öğlen: Ton Köftesi', status: false },
                  { food: 'Akşam: 3 adet yumurta (3 damla sıvı yağ ile  hazırlanmış) + Sade quark', status: false },
                  { food: 'Akşam: Patlican mezesi', status: false },
              ]
          },
          {
              day: '4. gün',
              content: [
                  { food: 'Kahvaltı: Soğanlı yoğurt sosu ile sebzeli börek', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Akşam: Kaşarlı tavuk köftesi', status: false },
                  { food: 'Akşam: Vanilyalı beze', status: false },
              ]
          },
          {
              day: '5. gün',
              content: [
                  { food: 'Kahvaltı: Balkabaklı mini krepler', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Öğlen: Pierre Dukan salatası', status: false },
                  { food: 'Akşam: Kabaklı havuçlu fırında mücver', status: false },
                  { food: 'Akşam: Ton balıklı meze', status: false },
              ]
          }
      ];
      this.consolidation = [
          {
              day: '1. gün',
              content: [
                  { food: 'Kahvaltı: Yarım elma ve 1 salkım üzüm', status: false },
                  { food: 'Kahvaltı: 2 yumurta', status: false },
                  { food: 'Kahvaltı: 1 dilim tam buğday ekmeği', status: false },
                  { food: 'Öğlen: Fırında hindili mantar', status: false },
                  { food: 'Öğlen: Chia salata', status: false },
                  { food: 'Akşam: Yeşil biberli tavuk dolması)', status: false },
                  { food: 'Akşam: 1 dilim tam buğday ekmeği', status: false },
              ]
          },
          {
              day: '2. gün',
              content: [
                  { food: 'Kahvaltı: 1 adet şeftali ve 1 adet nar', status: false },
                  { food: 'Kahvaltı: 1 dilim tam buğday ekmeği', status: false },
                  { food: 'Öğlen: Pepato, kremalı morbido brokoli', status: false },
                  { food: 'Akşam: Sebzeli mantarlı meze', status: false },
                  { food: 'Akşam: Kapari turşulu ton salata', status: false },
                  { food: 'Akşam: 1 dilim tam buğday ekmeği', status: false },
              ]
          },
          {
              day: '3. gün',
              content: [
                  { food: 'Kahvaltı: 3 adet erik ve 1 adet nektari', status: false },
                  { food: 'Kahvaltı: 1 dilim tam buğday ekmeği', status: false },
                  { food: 'Öğlen: Ton Köftesi', status: false },
                  { food: 'Akşam: 3 adet yumurta (3 damla sıvı yağ ile  hazırlanmış) + Sade quark', status: false },
                  { food: 'Akşam: Patlican mezesi', status: false },
                  { food: 'Akşam: 1 dilim tam buğday ekmeği', status: false },
              ]
          },
          {
              day: '4. gün',
              content: [
                  { food: 'Kahvaltı: 1 adet muz ve 1 adet armut', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Öğlen: 1 dilim tam buğday ekmeği', status: false },
                  { food: 'Akşam: Kaşarlı tavuk köftesi', status: false },
                  { food: 'Akşam: Vanilyalı beze', status: false },
                  { food: 'Akşam: 1 dilim tam buğday ekmeği', status: false },
              ]
          },
          {
              day: '5. gün',
              content: [
                  { food: 'Kahvaltı: 1 adet ayva ve 2 adet adet mandalina', status: false },
                  { food: 'Kahvaltı: 1 porsiyon peynir', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Öğlen: Pierre Dukan salatası', status: false },
                  { food: 'Öğlen: 1 dilim tam buğday ekmeği', status: false },
                  { food: 'Akşam: Kuzu eti', status: false },
                  { food: 'Akşam: Ton balıklı meze', status: false },
                  { food: 'Akşam: 1 dilim tam buğday ekmeği', status: false },
              ]
          }
      ];
      this.stabilisation = [
          {
              day: '1. gün',
              content: [
                  { food: 'Kahvaltı: Dukan Baharatlı Ekmek Fajita', status: false },
                  { food: 'Kahvaltı: 2 yumurta', status: false },
                  { food: 'Öğlen: Fırında hindili mantar', status: false },
                  { food: 'Öğlen: Chia salata', status: false },
                  { food: 'Akşam: Yeşil biberli tavuk dolması)', status: false },
                  { food: 'Akşam: Pişmiş kabak ve havuç', status: false },
                  { food: '20 dakika yürüyüş', status: false },
                  { food: 'Asansör ve yürüyen merdivenleri kullanmama', status: false },
              ]
          },
          {
              day: '2. gün',
              content: [
                  { food: 'Kahvaltı: Fajitas Tako Omlet', status: false },
                  { food: 'Öğlen: Pepato, kremalı morbido brokoli', status: false },
                  { food: 'Akşam: Sebzeli mantarlı meze', status: false },
                  { food: 'Akşam: Kapari turşulu ton salata', status: false },
                  { food: '20 dakika yürüyüş', status: false },
                  { food: 'Asansör ve yürüyen merdivenleri kullanmama', status: false },
              ]
          },
          {
              day: '3. gün',
              content: [
                  { food: 'Kahvaltı: 200 gr yağsız yoğurt + 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Öğlen: Ton Köftesi', status: false },
                  { food: 'Akşam: 3 adet yumurta (3 damla sıvı yağ ile  hazırlanmış) + Sade quark', status: false },
                  { food: 'Akşam: Patlican mezesi', status: false },
                  { food: '20 dakika yürüyüş', status: false },
                  { food: 'Asansör ve yürüyen merdivenleri kullanmama', status: false },
              ]
          },
          {
              day: '4. gün',
              content: [
                  { food: 'Kahvaltı: Soğanlı yoğurt sosu ile sebzeli börek', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Akşam: Kaşarlı tavuk köftesi', status: false },
                  { food: 'Akşam: Vanilyalı beze', status: false },
                  { food: '20 dakika yürüyüş', status: false },
                  { food: 'Asansör ve yürüyen merdivenleri kullanmama', status: false },
              ]
          },
          {
              day: '5. gün',
              content: [
                  { food: 'Kahvaltı: Balkabaklı mini krepler', status: false },
                  { food: 'Kahvaltı: 1,5 yemek kaşığı yulaf ezmesi', status: false },
                  { food: 'Kahvaltı: yağsız süt ile hazırlanmış omlet', status: false },
                  { food: 'Öğlen: Brownie tadında kakaolu kek', status: false },
                  { food: 'Öğlen: Pierre Dukan salatası', status: false },
                  { food: 'Akşam: Kabaklı havuçlu fırında mücver', status: false },
                  { food: 'Akşam: Ton balıklı meze', status: false },
                  { food: '20 dakika yürüyüş', status: false },
                  { food: 'Asansör ve yürüyen merdivenleri kullanmama', status: false },
              ]
          }
      ];
  }

}
