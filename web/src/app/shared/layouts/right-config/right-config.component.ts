import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import {Router} from '@angular/router';
import {DietService} from '../../services/diet.service';

@Component({
  selector: 'right-config',
  templateUrl: './right-config.component.html',
  styleUrls: ['./right-config.component.scss']
})
export class RightConfigComponent implements OnInit {

    user;

  isConfigToggle: boolean = false;
  constructor(private _globalService: GlobalService,
              private router: Router,
              private dietService: DietService) { }

  ngOnInit() {
      this.dietService.getUser(localStorage.getItem('username'))
          .subscribe(result => {
              this.user = result;
          });
  }

  configToggle() {
    this.isConfigToggle = !this.isConfigToggle;
    //this._globalService._sidebarToggleState(!this.isConfigToggle);
    this._globalService.dataBusChanged('sidebarToggle', !this.isConfigToggle);
  }

  isLoggedIn() {
      return localStorage.getItem('token') !== null;
  }

  onLoggedout() {
      localStorage.clear();
      this.configToggle();
      this.router.navigate(['/']);
  }
}
