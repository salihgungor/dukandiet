import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class DietService {

    constructor(private http: Http) {}

    createDiet(diet) {
        return this.http
            .post('https://dukandiet.herokuapp.com/users', { diet })
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    updateDiet(diet, username) {
      return this.http
        .patch('https://dukandiet.herokuapp.com/users/' + username, { diet })
        .map(response => {
          return response.json();
        })
        .catch(this.handleError);
    }

    login(user) {
        return this.http
            .post('https://dukandiet.herokuapp.com/users/login', { user })
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getUser(username) {
        return this.http
            .get('https://dukandiet.herokuapp.com/users/' + username)
            .map(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.error('DietService::handleError', error);
        return Observable.throw(error);
    }

}
