const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');

const User = require('../models/user');

/* GET users listing. */
router.get('/:username', async (req, res, next) => {
  const user = await User.findOne({username: req.params.username}).lean();

  res.json(user);
});

router.post('/', async (req, res, next) => {
    const user = await new User(req.body.diet);
    user.save();

    res.json({message: 'Kayıt işlemi başarılı artık sağlıklı bir diyete hazırız.'});
});

router.patch('/:username', async (req, res, next) => {
  const user = await User.findOne({username: req.params.username});
  user.diet = await req.body.diet.diet;
  user.save();

  res.json(user);
});

router.post('/login', async (req, res, next) => {
    User.findOne({username: req.body.user.username}, (err, user) => {
        if (err)
            return res.status(500).json({
                title: 'An error occurred.',
                error: err
            });
        if (!user)
            return res.status(401).json({
                title: 'Login failed',
                error: {message: 'İnvalid login credentials'}
            });
        if (user.password != req.body.user.password) {
            return res.status(401).json({
                title: 'not compare password',
                error: {message: 'İnvalid login credentials'}
            });
        }
        var token = jwt.sign({user}, 'secret', {expiresIn: 7200});
        res.status(200).json({
            message: 'Successfully logged in',
            token: token,
            userId: user._id,
            name: user.name,
            username: user.username
        });
    });
});

module.exports = router;
