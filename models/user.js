const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const UserSchema = new Schema(
    {
        name: String,
        lastName: String,
        email: String,
        gender: String,
        birthData: Date,
        height: String,
        nowWeight: Number,
        targetWeight: Number,
        highestWeight: Number,
        lowestWeight: Number,
        averageWeight: Number,
        familyFat: String,
        howManyDietBefore: String,
        username: String,
        password: String,
        startingDate: Date,
        diet: []

    },
    { strict: false }
);

module.exports = mongoose.model('user', UserSchema);

// {
//     atack: [{ day: String, content: [], status: Boolean }]
// },
// {
//     cruise: [{ day: String, content: [], status: Boolean }]
// },
// {
//     consolidation: [{ day: String, content: [], status: Boolean }]
// },
// {
//     stabilisation: [{ day: String, content: [], status: Boolean }]
// }
